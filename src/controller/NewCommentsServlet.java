package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comments;
import beans.User;
import service.CommentsService;

@WebServlet(urlPatterns = { "/newComments" })
public class NewCommentsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {


		HttpSession session = request.getSession();
		List<String> Comments = new ArrayList<String>();
		Comments Comment = new Comments();

		if (isValid(request, Comments) == true) {
		User user = (User) session.getAttribute("loginUser");
		Comment.setText(request.getParameter("Comments"));
		Comment.setCommentsId(request.getParameter("CommentsId"));
		Comment.setUserId(user.getId());
		new CommentsService().register(Comment);
		response.sendRedirect("./");
//		request.getRequestDispatcher("/hame.jsp").forward(request, response);
		} else {
	        session.setAttribute("errorMessages", Comments);

			response.sendRedirect("./");
//			request.getRequestDispatcher("/home.jsp").forward(request, response);
		}

	}

	    private boolean isValid(HttpServletRequest request, List<String> Comments) {

	        String Comment = request.getParameter("Comments");
	        if (StringUtils.isBlank(Comment) == true){
	        	 Comments.add("コメントを入力してください");
	        }
	        if (500 < Comment.length()) {
	            Comments.add("コメントは500文字以下で入力してください");
	        }
	        if (Comments.size() == 0) {
	            return true;
	        } else {
	            return false;
	        }
	    }

}