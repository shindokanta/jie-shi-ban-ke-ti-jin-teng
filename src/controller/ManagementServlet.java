package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserManagement;
import service.ManagementService;

@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet  extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {


		List<String> message = new ArrayList<String>();
		UserManagement management = new UserManagement();


		List<UserManagement> managements = new ManagementService().getUserManagement();
		management.setName(request.getParameter("management"));

		request.setAttribute("management", managements);

		request.getRequestDispatcher("management.jsp").forward(request, response);

	}
}
