package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Comments;
import beans.Message;
import service.DeleteCommentsService;
import service.DeleteMessageService;

@WebServlet(urlPatterns = { "/delete" })
public class DeleteCommentsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {


		if (request.getParameter("CommentsId") != null){
			Comments Comment = new Comments();

			Comment.setCommentsId(request.getParameter("CommentsId"));
			new DeleteCommentsService().register(Comment);
			response.sendRedirect("./");
			return;
		}
		Message Message = new Message();

		Message.setMessageId(request.getParameter("MessageId"));
		new DeleteMessageService().register(Message);
		response.sendRedirect("./");


	}
}