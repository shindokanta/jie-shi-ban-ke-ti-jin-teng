package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import beans.UserManagement;
import service.UserStatusResetService;
import service.UserStatusService;

@WebServlet(urlPatterns = { "/userStatus" })
public class UserStatusServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		int ui = Integer.parseInt(request.getParameter("UserStatus"));
		int mi = Integer.parseInt(request.getParameter("MessageId"));


		User userStatus = (User) request.getSession().getAttribute("loginUser");

		if (userStatus == null) {

			response.sendRedirect("login");
			return;
		}


		if (ui == 0){
			UserManagement user = new UserManagement();

			user.setId(mi);

			new UserStatusService().register(user);
			response.sendRedirect("management");
			return;
		}else{
			UserManagement user = new UserManagement();

			user.setId(mi);

			new UserStatusResetService().register(user);
			response.sendRedirect("management");

		}

	}

}