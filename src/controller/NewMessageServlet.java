package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();



		if (isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");
			Message message = new Message();
			message.setSubject(request.getParameter("subject"));
			message.setText(request.getParameter("message"));
			message.setCategory(request.getParameter("category"));
			message.setUserId(user.getId());
			new MessageService().register(message);
			response.sendRedirect("./");
		} else {
			request.setAttribute("subject", request.getParameter("subject"));
			request.setAttribute("message", request.getParameter("message"));
			request.setAttribute("category", request.getParameter("category"));
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("/messages.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String subject = request.getParameter("subject");
		String message = request.getParameter("message");
		String category = request.getParameter("category");


		if (StringUtils.isBlank(subject) == true) {
			messages.add("件名を入力してください");
		}
		if (30 < subject.length()) {
			messages.add("件名は30文字以下で入力してください");
		}
		if (StringUtils.isBlank(message) == true ) {
			messages.add("本文を入力してください");
		}
		if (1000 < message.length()) {
			messages.add("本文は1000文字以下で入力してください");
		}
		if (StringUtils.isBlank(category) == true ) {
			messages.add("カテゴリーを入力してください");
		}
		if (10 < category.length()) {
			messages.add("カテゴリーは10文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}