package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserComments;
import beans.UserMessage;
import service.CommentsService;
import service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> message = new ArrayList<String>();

		String startday = request.getParameter("startday");
		String endday = request.getParameter("endday");
		String serch = request.getParameter("serch");

		if (startday == null || startday.isEmpty()) {
			startday = "2018-01-01";
		}
		if (endday == null || endday.isEmpty()) {
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			endday = sdf.format(date.getTime()).toString();
		}

		if (serch == null || serch.isEmpty()){
			serch = "%";
		}
		if ( 10 < serch.length()){
			serch = "%";
		}
		List<UserMessage> messages = new MessageService().getMessage(startday, endday, serch);
		List<UserComments> comments = new CommentsService().getComments();


		if (isValid(request, message) == true) {
			if (request.getParameter("startday") != null || request.getParameter("endday") != null) {
				request.setAttribute("startday", startday);
				request.setAttribute("endday", endday);
			}
			request.setAttribute("messages", messages);
			request.setAttribute("comments", comments);
			request.getRequestDispatcher("/home.jsp").forward(request, response);
		} else {
			request.setAttribute("messages", messages);
			request.setAttribute("comments", comments);
			request.setAttribute("errorMessages",message );
			request.getRequestDispatcher("/home.jsp").forward(request, response);
		}


	}
	private boolean isValid(HttpServletRequest request, List<String> message) {
		String serch = request.getParameter("serch");

		if (serch == null) {
			return true;
		}
		if (10 < serch.length() == true) {
			message.add("カテゴリーは10字以内で入力してください");
		}
		if (message.size() == 0) {
			return true;
		} else {
			return false;
		}

	}
}
