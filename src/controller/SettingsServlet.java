package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		if(request.getParameter("settings") == null && request.getParameter("empid") == null) {
			messages.add("不正な値です");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
			return;
		}
		if(request.getParameter("empid").matches("^[0-9]{1,10}$")==false || request.getParameter("empid") == null || request.getParameter("empid") == ""){
			messages.add("不正な値です");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
			return;
		}

		int i = Integer.parseInt(request.getParameter("empid"));

		User editUser = new UserService().getUser(i);

		if(editUser != null) {
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
		} else {
			messages.add("不正な値です");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
		}


	}
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {


		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);
		session.setAttribute("editUser", editUser);

		if (isValid(request, messages) == true) {
			new UserService().update(editUser);
			request.setAttribute("editUser", editUser);
			response.sendRedirect("management");
		}else {
			request.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setName(request.getParameter("name"));
		editUser.setAccount(request.getParameter("account"));
		editUser.setPassword(request.getParameter("password"));
		if (request.getParameter("branch_id") == null) {
			editUser.setBranch_id(0);
		} else {
			int branch_id = Integer.parseInt(request.getParameter("branch_id"));
			editUser.setBranch_id(branch_id);
		}
		if (request.getParameter("position_id") == null) {
			editUser.setPosition_id(0);
		} else {
			int position_id = Integer.parseInt(request.getParameter("position_id"));
			editUser.setPosition_id(position_id);

		}
		return editUser;
	}


	private boolean isValid(HttpServletRequest request, List<String> messages) {

		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String checkpassword = request.getParameter("checkPassword");




		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		}
		if (10 < name.length()) {
			messages.add("名前は10文字以下で入力してください");
		}
		if (StringUtils.isEmpty(account) == true) {
			messages.add("ログインIDを入力してください");
		}
		if (20 < account.length() || 6 > account.length()) {
			messages.add("ログインIDは6文字以上20文字以下で入力してください");
		}
		if (StringUtils.isEmpty(password) == false) {
			if (20 < password.length() || 6 > password.length()) {
				messages.add("パスワードは6文字以上20文字以下で入力してください");
			}
		}
		if (StringUtils.isEmpty(account) == false) {
			User editUser = new UserService().getUserAccunt(account);
			if (editUser != null ) {
				int userID = editUser.getId();
				if(userID != id ){
					messages.add("同じログインIDが存在します");
				}
			}
		}
		if (!password.equals(checkpassword)) {
			messages.add("確認用パスワードが一致しません");
		}
		if (request.getParameter("branch_id") != null && request.getParameter("position_id") != null) {
			int branch_id = Integer.parseInt(request.getParameter("branch_id"));
			int position_id = Integer.parseInt(request.getParameter("position_id"));
			if ( branch_id == 1 && position_id > 2 ) {
				messages.add("不正な組み合わせです");
			}
		}
		if (request.getParameter("branch_id") != null && request.getParameter("position_id") != null) {
			int branch_id = Integer.parseInt(request.getParameter("branch_id"));
			int position_id = Integer.parseInt(request.getParameter("position_id"));
			if ( branch_id > 1  && position_id <= 2 ) {
				messages.add("不正な組み合わせです");
			}
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}