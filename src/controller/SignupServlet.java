package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = {"/signup"})
public class SignupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {


		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		User user = new User();
		user.setName(request.getParameter("name"));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		int branch_id = Integer.parseInt(request.getParameter("branch_id"));
		user.setBranch_id(branch_id);
		int position_id = Integer.parseInt(request.getParameter("position_id"));
		user.setPosition_id(position_id);

		if (isValid(request, messages) == true) {

			new UserService().regster(user);
			response.sendRedirect("management");
		} else {
			request.setAttribute("name" , request.getParameter("name"));
			request.setAttribute("account" , request.getParameter("account"));
			request.setAttribute("errorMessages" , messages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String name = request.getParameter("name");
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String checkPassword = request.getParameter("checkPassword");
		int branch_id = Integer.parseInt(request.getParameter("branch_id"));
		int position_id = Integer.parseInt(request.getParameter("position_id"));


		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		}
		if (!StringUtils.isEmpty(name) == true) {
			if (10 < name.length()) {
				messages.add("名前は10文字以下で入力してください");
		}
		}
		if (StringUtils.isEmpty(account) == true) {
			messages.add("ログインIDを入力してください");
		}
		if (!StringUtils.isEmpty(account) == true) {
		if (20 < account.length() || 6 > account.length()) {
			messages.add("ログインIDは6文字以上20文字以下で入力してください");
		}
		}
		if (StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		}
		if (!StringUtils.isEmpty(password) == true) {
		if (20 < password.length() || 6 > password.length()) {
			messages.add("パスワードは6文字以上20文字以下で入力してください");
		}
		}
		if (StringUtils.isEmpty(account) == false) {
			User editUser = new UserService().getUserAccunt(account);
			if (editUser != null ) {
			messages.add("同じログインIDが存在します");
			}
		}
		if (!password.equals(checkPassword)) {
			messages.add("確認用パスワードが一致しません");
		}
		if (StringUtils.isEmpty(request.getParameter("branch_id")) == true) {
			messages.add("支店を選択してください");
		}
		if (StringUtils.isEmpty(request.getParameter("position_id")) == true) {
			messages.add("役職を選択してください");
		}
		if ( branch_id == 1 && position_id > 2 ) {
			messages.add("不正な組み合わせです");
		}
		if ( branch_id > 1  && position_id <= 2 ) {
			messages.add("不正な組み合わせです");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}

	}



}