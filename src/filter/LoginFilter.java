package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter("/*")
public class LoginFilter implements Filter {


	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {


		String SP = ((HttpServletRequest)request).getServletPath();
		HttpServletResponse HSR = ((HttpServletResponse)response);
		HttpSession session = ((HttpServletRequest)request).getSession();
		List<String> messages = new ArrayList<String>();

		if(SP.equals("/login") && session.getAttribute("loginUser") != null){
			HSR.sendRedirect("./");
			return;
		}


		if(SP.equals("/login") || session.getAttribute("loginUser") != null){
			chain.doFilter(request, response);
		}else{
			messages.add("ログインしてください");
			session.setAttribute("errorMessages", messages);
			HSR.sendRedirect("login");
		}
	}



	@Override
	public void destroy() {
		// TODO 自動生成されたメソッド・スタブ

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO 自動生成されたメソッド・スタブ

	}

}