package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.UserService;


@WebFilter("/*")
public class AuthorityFilter implements Filter {


	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {


		String SP = ((HttpServletRequest)request).getServletPath();
		HttpSession session = ((HttpServletRequest)request).getSession();
		HttpServletResponse HSR = ((HttpServletResponse)response);
		List<String> messages = new ArrayList<String>();
		User userStatus = (User)session.getAttribute("loginUser");



		if (session.getAttribute("loginUser") == null) {
			chain.doFilter(request, response);
			return;
		}


		User editUser = new UserService().getUser(userStatus.getId());

		if (editUser.getUser_status() == 1) {
			session.invalidate();
			HSR.sendRedirect("login");
			return;
		}



		if(SP.equals("/signup") && userStatus.getPosition_id() != 1){
			messages.add("権限がありません");
			session.setAttribute("errorMessages", messages);
			HSR.sendRedirect("./");
		}else if(SP.equals("/management") && userStatus.getPosition_id() != 1){
			messages.add("権限がありません");
			session.setAttribute("errorMessages", messages);
			HSR.sendRedirect("./");
		}else if(SP.equals("/settings") && userStatus.getPosition_id() != 1){
			messages.add("権限がありません");
			session.setAttribute("errorMessages", messages);
			HSR.sendRedirect("./");
		}else{
			chain.doFilter(request, response);
		}
	}



	@Override
	public void destroy() {
		// TODO 自動生成されたメソッド・スタブ

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO 自動生成されたメソッド・スタブ

	}

}