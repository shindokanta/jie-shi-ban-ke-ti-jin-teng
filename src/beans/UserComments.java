package beans;

import java.io.Serializable;
import java.util.Date;

public class UserComments implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String name;
	private int userId;
	private String text;
	private Date created_date;
	private int messagesId;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public void setMessagesId(int messagesId) {
		this.messagesId = messagesId;
	}
	public int getMessagesId() {
		return messagesId;
	}
}