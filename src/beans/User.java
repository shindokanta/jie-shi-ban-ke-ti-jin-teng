package beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
	public static final long serialVersionUID = 1L;

	private int id;
	private String account;
	private String name;
	private String password;
	private int  branch_id;
	private int  position_id;
	private int  user_status;
	private Date createdDate;
	private Date updatedDate;


	public void setId(int id){
		this.id = id;
	}
	public void setAccount(String account){
		this.account = account;
	}
	public void setName(String name){
		this.name = name;
	}
	public void setPassword(String password){
		this.password = password;
	}
	public void setBranch_id(int branch_id){
		this.branch_id = branch_id;
	}
	public void setPosition_id(int position_id){
		this.position_id = position_id;
	}
	public void setUpdatedDate(Date updatedDate){
		this.updatedDate = updatedDate;
	}
	public void setCreatedDate(Date createdDate){
		this.createdDate = createdDate;
	}
	public void setUser_status(int user_status){
		this.user_status = user_status;
	}
	public String getName(){
		return name;
	}
	public String getAccount(){
		return account;
	}
	public String getPassword(){
		return password;
	}
	public int getBranch_id(){
		return branch_id;
	}
	public int getPosition_id(){
		return position_id;
	}
	public Date getCreatedDate(){
		return createdDate;
	}
	public Date getUpdatedDate(){
		return updatedDate;
	}
	public int getId(){
		return id;
	}
	public int getUser_status(){
		return user_status;
	}

}
