package beans;

public class UserManagement {

	private int id;
	private int User_status;
	private String Account;
	private String Name;
	private String Branch_id;
	private String Position_id;

	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	public void setUser_status(int User_status) {
		this.User_status = User_status;
	}
	public int getUser_status() {
		return User_status;
	}
	public void setAccount(String Account) {
		this.Account = Account;
	}
	public String getAccount() {
		return Account;
	}
	public void setName(String Name) {
		this.Name = Name;
	}
	public String getName() {
		return Name;
	}
	public void setBranch_id(String Branch_id) {
		this.Branch_id = Branch_id;
	}
	public String getBranch_id() {
		return Branch_id;
	}
	public void setPosition_id(String Position_id) {
		this.Position_id = Position_id;
	}
	public String getPosition_id() {
		return Position_id;
	}

}
