package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.Comments;
import dao.DeleteCommentsDao;

public class DeleteCommentsService {

	public void register(Comments comments) {

		Connection connection = null;
		try {
			connection = getConnection();

			DeleteCommentsDao DeleteCommentsDao = new DeleteCommentsDao();
			DeleteCommentsDao.delete(connection, comments);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}