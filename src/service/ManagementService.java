package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.UserManagement;
import dao.ManagementDao;


public class ManagementService {

	private static final int LIMIT_NUM = 1000;

	public List<UserManagement> getUserManagement() {

		Connection connection = null;
		try {
			connection = getConnection();

			ManagementDao ManagementDao = new ManagementDao();
			List<UserManagement> ret = ManagementDao.getUserManagement(connection, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}



}
