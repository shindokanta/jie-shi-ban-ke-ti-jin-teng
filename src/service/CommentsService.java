package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Comments;
import beans.UserComments;
import dao.CommentsDao;
import dao.UserCommentsDao;

public class CommentsService {

	public void register(Comments comments) {

		Connection connection = null;
		try {
			connection = getConnection();

			CommentsDao CommentsDao = new CommentsDao();
			CommentsDao.insert(connection, comments);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	private static final int LIMIT_NUM = 1000;

	public List<UserComments> getComments() {

		Connection connection = null;
		try {
			connection = getConnection();

			UserCommentsDao CommentsDao = new UserCommentsDao();
			List<UserComments> ret = CommentsDao.getUserComments(connection, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}

}