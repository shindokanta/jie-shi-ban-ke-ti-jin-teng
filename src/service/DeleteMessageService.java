package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.Message;
import dao.DeleteMessageDao;

public class DeleteMessageService {

	public void register(Message Message) {

		Connection connection = null;
		try {
			connection = getConnection();

			DeleteMessageDao DeleteMessageDao = new DeleteMessageDao();
			DeleteMessageDao.delete(connection, Message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}