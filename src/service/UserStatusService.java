package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.UserManagement;
import dao.UserStatusDao;

public class UserStatusService {

	public void register(UserManagement user) {


		Connection connection = null;
		try {
			connection = getConnection();

			UserStatusDao userStatusDao = new UserStatusDao();
			userStatusDao.update(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
