package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserComments;
import exception.SQLRuntimeException;

public class UserCommentsDao {

	public List<UserComments> getUserComments(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id as id, ");
			sql.append("comments.text as text, ");
			sql.append("comments.messages_id as messages_id, ");
			sql.append("comments.user_id as user_id, ");
			sql.append("users.account as account, ");
			sql.append("users.name as name, ");
			sql.append("comments.created_date as created_date ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY created_date ASC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserComments> ret = toUserCommentsList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}



	private List<UserComments> toUserCommentsList(ResultSet rs)
			throws SQLException {

		List<UserComments> ret = new ArrayList<UserComments>();
		try {
			while (rs.next()) {
				String name = rs.getString("name");
				int id = rs.getInt("id");
				int messagesId = rs.getInt("messages_id");
				int userId = rs.getInt("user_id");
				String text = rs.getString("text");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserComments message = new UserComments();
				message.setName(name);
				message.setId(id);
				message.setMessagesId(messagesId);
				message.setUserId(userId);
				message.setText(text);
				message.setCreated_date(createdDate);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}