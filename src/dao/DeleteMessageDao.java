package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Message;
import exception.SQLRuntimeException;

public class DeleteMessageDao {

	public void delete(Connection connection, Message Message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM messages  ");
			sql.append(" WHERE (");
			sql.append(" id =");
			sql.append("?");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, Message.getMessageId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}