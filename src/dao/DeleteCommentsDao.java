package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Comments;
import exception.SQLRuntimeException;

public class DeleteCommentsDao {

	public void delete(Connection connection, Comments Comments) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM comments  ");
			sql.append(" WHERE (");
			sql.append(" id =");
			sql.append("?");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, Comments.getCommentsId());


			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}