package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserManagement;
import exception.SQLRuntimeException;

public class ManagementDao {

	public List<UserManagement> getUserManagement(Connection connection, int num) {



		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.account as account, ");
			sql.append("users.name as name, ");
			sql.append("users.user_status as user_status, ");
			sql.append("branches.name as branches, ");
			sql.append("positions.name as positions ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN positions ");
			sql.append("ON users.position_id = positions.id ");
			sql.append("ORDER BY users.id ASC ");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<UserManagement> ret = toUserManagementList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	private List<UserManagement> toUserManagementList(ResultSet rs)
			throws SQLException {

		List<UserManagement> ret = new ArrayList<UserManagement>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int user_status = rs.getInt("user_status");
				String account = rs.getString("account");
				String name = rs.getString("name");
				String branches = rs.getString("branches");
				String positions = rs.getString("positions");

				UserManagement Management = new UserManagement();
				Management.setId(id);
				Management.setUser_status(user_status);
				Management.setAccount(account);
				Management.setName(name);
				Management.setBranch_id(branches);
				Management.setPosition_id(positions);

				ret.add(Management);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
