<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社内掲示板</title>
<link rel="stylesheet" type="text/css" href="style.css">
<script type="text/javascript">
<!--

function check(){

	if(window.confirm('送信してよろしいですか？')){ // 確認ダイアログを表示

		return true; // 「OK」時は送信を実行

	}
	else{ // 「キャンセル」時の処理

		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false; // 送信を中止

	}

}

// -->
</script>

</head>

<body style="word-break: break-all;">
	<div class="header">
		<div class="home">
			<a href="./">
			<img src="home.png" alt="home" width="50" height="50">ホーム
			</a>
			<h1 id="top">
				社内掲示板</h1>
		</div>
		<div class="sonota">
			<c:if test="${ not empty loginUser }">
				ようこそ！
					<c:out value="${loginUser.name}" />さん！
			</c:if>
			<c:if test="${loginUser.branch_id == 1 }">
				<a href="management" class="management">
					<img src="management.png" alt="management" width="50" height="50">ユーザー管理</a>
			</c:if>
			<a href="messages" class="messagenenu">
				<img src="message.png" alt="message" width="50" height="50">新規投稿</a>
					<a href="logout" class="iogout">
						<img src="logout.png" alt="logout" width="50" height="50">ログアウト</a>
		</div>
	</div>
	<br>


	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li class="errorMessage"><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
	</div>
	<div class="sertch">
		<form action="./" method="get">
			<input type="date" name="startday" value="${startday}" id="startday">
			から <input type="date" name="endday" value="${endday}" id="endday"><br>
			<input type=text name="serch" id="serch" placeholder="カテゴリー"><br>
			<input type="submit" value="検索する"><br>
		</form>
		<form action="./" method="get">
			<input type="submit" name="reset" value="リセット"
				style="color: #FF0000;">
		</form>

	</div>


	<c:if test="${ not empty loginUser }">
		<br>
		<c:forEach var="message" items="${messages}">
			<div class="message">
				<div class="subject">
					件名：
					 <c:out value="${message.subject}" />
				</div>
			<div class="date">
					<span class="name">投稿者：<c:out value="${message.name}" /></span>
				</div>
									<div class="textcss">
				<c:forEach var="texts" items="${fn:split(message.text, '
				')}">
					<div>
						<c:out value="${texts}" />
					</div>
				</c:forEach>
</div>
				<div class="category">
					カテゴリー：
					<c:out value="${message.category}" />
				</div>
				<div class="account-name">
										投稿日時：
					<fmt:formatDate value="${message.created_date}"
						pattern="yyyy/MM/dd HH:mm:ss" />				</div>

				<form action="delete" method="post" onSubmit="return check()">
					<c:if test="${loginUser.id == message.userId }">
						<input name="MessageId" value="${message.id}" id="MessageId"
							type="hidden" />
						<input type="submit" value="投稿を削除する" style="color: #FF0000;" >
					</c:if>
				</form>
				<br>
				<c:forEach items="${comments}" var="comment">
					<c:if test="${comment.messagesId == message.id }">
						<div class="message2">
						<div class="commenttitle">
						コメント
						</div>
						<div class="comments">
							<c:forEach var="comments"
								items="${fn:split(comment.text, '
				')}">
								<div class="text">

									<c:out value="${comments}" />
								</div>
							</c:forEach>
							</div>
							<div class="account-name">
								<span class="name">コメント投稿者：<c:out value="${comment.name}" /></span>
							</div>
							<div class="date">
								投稿日時：
								<fmt:formatDate value="${comment.created_date}"
									pattern="yyyy/MM/dd HH:mm:ss" />
							</div>
							<c:if test="${loginUser.id == comment.userId }">
								<form action="delete" method="post" onSubmit="return check()">
									<input name="CommentsId" value="${comment.id}" id="CommentsId"
										type="hidden" /> <input type="submit" value="コメントを削除する"
										style="color: #FF0000;">
								</form>
							</c:if>
						</div>
						<br>
					</c:if>
				</c:forEach>
				<form action="newComments" method="post">
					<br /> <input name="CommentsId" value="${message.id}"
						id="CommentsId" type="hidden" />
					<textarea name="Comments" cols="70" rows="5" class="tweet-box"></textarea>
					<br /> <input type="submit" value="投稿にコメントする">500字まで
				</form>
			</div>
			<br>
		</c:forEach>
	</c:if>
</body>
</html>