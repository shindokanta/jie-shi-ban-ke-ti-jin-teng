<%--
    Author     : tool-taro.com
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>header</title>
</head>
<body>
<div class ="header">
	<a href="./">ホーム</a>
	<c:if test="${loginUser.branch_id == 1 }">
		<a href="management">ユーザー管理</a>
	</c:if>
	<a href="messages">新規投稿</a>
	<a href="logout">ログアウト</a>
</div>
</body>
</html>