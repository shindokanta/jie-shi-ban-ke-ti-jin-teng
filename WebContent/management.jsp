<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
<link rel="stylesheet" type="text/css" href="style.css">
<script type="text/javascript">
<!--
	function check() {

		if (window.confirm('送信してよろしいですか？')) { // 確認ダイアログを表示

			return true; // 「OK」時は送信を実行

		} else { // 「キャンセル」時の処理

			window.alert('キャンセルされました'); // 警告ダイアログを表示
			return false; // 送信を中止

		}

	}
// -->
</script>

</head>
<body>

<div class="header">
<div class="home">
	<a href="./" ><img src="home.png" alt="home" width="50" height="50">ホーム</a>
	<h1 id="top">社内掲示板 - ユーザー管理</h1>
</div>
<div class="sonota">

<c:if test="${ not empty loginUser }">
					ようこそ！<c:out value="${loginUser.name}" />さん！
	</c:if>

	<c:if test="${loginUser.branch_id == 1 }">
		<a href="signup" class="signup"><img src="signup.png" alt="signup" width="50" height="50">ユーザー登録</a>
	</c:if>
	<a href="messages" class="messagenenu" ><img src="message.png" alt="message" width="50" height="50">新規投稿</a>
	<a href="logout" class="iogout"><img src="logout.png" alt="logout" width="50" height="50">ログアウト</a>
</div>
</div>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li class="errorMessage"><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

	</div>

<div>
	<c:if test="${ not empty loginUser }">
		<table  border="1" align="left" class="tableCss">
			<tr>
				<th>名前</th>
				<th>ログインID</th>
				<th>所属</th>
				<th>役職</th>
				<th>アカウント状態</th>
			</tr>
			<c:forEach items="${management}" var="management">
				<tr>
					<td><c:out value="${management.name}" /></td>
					<td><c:out value="${management.account}" /></td>
					<td><c:out value="${management.branch_id}" /></td>
					<td><c:out value="${management.position_id}" /></td>
					<c:if test="${management.user_status == 0 }">
						<td><a>通常</a></td>
					</c:if>
					<c:if test="${management.user_status == 1}">
						<td><a style="color: #FF0000;">停止中</a></td>
					</c:if>
					<c:if test="${management.position_id == 総務人事担当者}">
						<td><a></a></td>
					</c:if>
					<td><c:if test="${management.id != loginUser.id }">
							<form action="userStatus" method="post" onSubmit="return check()">
								<input name="MessageId" value="${management.id}" id="MessageId"
									type="hidden" /> <input name="UserStatus"
									value="${management.user_status}" id="UserStatus" type="hidden" />
								<c:if test="${ management.user_status == 0 }">
									<input type="submit" value="アカウントを停止" style="color: #FF0000;">
								</c:if>
								<c:if test="${ management.user_status ==  1}">
									<input type="submit" value="停止解除">
								</c:if>
							</form>
						</c:if></td>
					<td><a href="settings?empid=${management.id}">編集 </a></td>
				</tr>
			</c:forEach>
		</table>
	</c:if>
</div>
</body>
</html>
