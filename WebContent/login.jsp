<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title >ログイン</title>
<link rel="stylesheet" type="text/css" href="style.css">

</head>
<body>
	<div >
		<h1 style="margin: 0px 500px 0px 500px;">ログイン</h1>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages" style="margin: 0px 500px 0px 500px;">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li style="color: #FF0000;"><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
<div class="logincss" style="background-color: #DDA0DD;
margin: 0px 500px 0px 500px;
padding: 20px;
-webkit-border-top-left-radius: 20px;
    -webkit-border-top-right-radius: 20px;
    -webkit-border-bottom-right-radius: 20px;
    -webkit-border-bottom-left-radius: 20px;">
		<form action="login" method="post"><br />
			<label for="account" ></label><input name="account" value="${account}" id="account" placeholder="ログインID"/> <br />
			<label for="password" ></label><input name="password" type="password" id="password" placeholder="パスワード"/> <br /> <input
				type="submit" value="ログイン" /> <br />
		</form>
		</div>
	</div>
</body>
</html>