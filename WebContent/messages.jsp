<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿</title>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

<div class="header">
<div class="home">
	<a href="./" ><img src="home.png" alt="home" width="50" height="50">ホーム</a>
	<h1 id="top">社内掲示板 - 新規投稿</h1>
</div>
<div class="sonota">

<c:if test="${ not empty loginUser }">
					ようこそ！<c:out value="${loginUser.name}" />さん！
	</c:if>

	<c:if test="${loginUser.branch_id == 1 }">
			<a href="management" class="management"><img src="management.png" alt="management" width="50" height="50">ユーザー管理</a>
	</c:if>

	<a href="logout" class="iogout"><img src="logout.png" alt="logout" width="50" height="50">ログアウト</a>
</div>
</div>


	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li class="errorMessage"><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		</div>
		<div class="signupcss">

				<form action="newMessage" method="post"><br />
				<a>件名</a>
				<input name="subject" value="${subject}" ></input>(30字まで)<br>
				<a>本文</a>
				<textarea name="message" cols="80" rows="5" class="tweet-box">${message}</textarea>(1000字まで)<br>
				<a>カテゴリー</a>
				<input name="category" value="${category}"></input>(10字まで)<br />
				<input type="submit" value="投稿">
				</form>

		</div>
</body>
</html>