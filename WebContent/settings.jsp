<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${loginUser.account}の設定</title>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

<div class="header">
<div class="home">
	<a href="./" ><img src="home.png" alt="home" width="50" height="50">ホーム</a>
	<h1 id="top">社内掲示板 - ユーザー編集</h1>
</div>
<div class="sonota">

<c:if test="${ not empty loginUser }">
					ようこそ！<c:out value="${loginUser.name}" />さん！
	</c:if>

	<c:if test="${loginUser.branch_id == 1 }">
		<a href="management" class="management"><img src="management.png" alt="management" width="50" height="50">ユーザー管理</a>
	</c:if>
	<a href="messages" class="messagenenu" ><img src="message.png" alt="message" width="50" height="50">新規投稿</a>
	<a href="logout" class="iogout"><img src="logout.png" alt="logout" width="50" height="50">ログアウト</a>
</div>
</div>

	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li class="errorMessage"><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="settings" method="post" class="signupcss">
			<br />
			<input name="id" value="${editUser.id}" id="id" type="hidden" />
			<label for="name">名前</label>
			<input name="name" value="${editUser.name}" id="name" />(10文字以下)（名前はあなたの公開プロフィールに表示されます）<br />
			<label for="account">ログインID</label>
			<input name="account" value="${editUser.account}" />(半角英数字6～20文字以下)<br />
			<label for="password">パスワード</label>
			<input name="password" type="password" id="password" />(半角英数字6～20文字以下) <br />
			<label for="checkPassword">確認用パスワード</label>
			<input name="checkPassword" type="password" id="checkPassword" /> <br />
<c:if test="${loginUser.id != editUser.id}">
			<label for="branch">所属</label>
			<SELECT NAME="branch_id" >
				<c:if test="${editUser.branch_id == 1}"><OPTION VALUE="1" selected>本社</c:if>
				<c:if test="${editUser.branch_id != 1}"><OPTION VALUE="1" >本社</c:if>
				<c:if test="${editUser.branch_id == 2}"><OPTION VALUE="2" selected>東京支店</c:if>
				<c:if test="${editUser.branch_id != 2}"><OPTION VALUE="2" >東京支店</c:if>
				<c:if test="${editUser.branch_id == 3}"><OPTION VALUE="3" selected>大阪支店</c:if>
				<c:if test="${editUser.branch_id != 3}"><OPTION VALUE="3" >大阪支店</c:if>
				<c:if test="${editUser.branch_id == 4}"><OPTION VALUE="4" selected>鳥取支店</c:if>
				<c:if test="${editUser.branch_id != 4}"><OPTION VALUE="4" >鳥取支店</c:if>
				<c:if test="${editUser.branch_id == 5}"><OPTION VALUE="5" selected>雑色支店</c:if>
				<c:if test="${editUser.branch_id != 5}"><OPTION VALUE="5" >雑色支店</c:if>
			</SELECT><br>
			<label for="post">部署・役職</label>
			<SELECT  NAME="position_id">
				<c:if test="${editUser.position_id == 1}"><OPTION VALUE="1" selected>総務人事担当者</c:if>
				<c:if test="${editUser.position_id != 1}"><OPTION VALUE="1" >総務人事担当者</c:if>
				<c:if test="${editUser.position_id == 2}"><OPTION VALUE="2" selected>情報管理担当者</c:if>
				<c:if test="${editUser.position_id != 2}"><OPTION VALUE="2" >情報管理担当者</c:if>
				<c:if test="${editUser.position_id == 3}"><OPTION VALUE="3" selected>支店長</c:if>
				<c:if test="${editUser.position_id != 3}"><OPTION VALUE="3" >支店長</c:if>
				<c:if test="${editUser.position_id == 4}"><OPTION VALUE="4" selected>社員１</c:if>
				<c:if test="${editUser.position_id != 4}"><OPTION VALUE="4" >社員１</c:if>
				<c:if test="${editUser.position_id == 5}"><OPTION VALUE="5" selected>社員２</c:if>
				<c:if test="${editUser.position_id != 5}"><OPTION VALUE="5" >社員２</c:if>
			</SELECT>
</c:if><input type="submit" value="登録" /> <br />
		</form>
	</div>
</body>
</html>